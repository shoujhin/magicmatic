<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" type="text/css" href="http://<?= $_SERVER['HTTP_HOST'] ?>/public/css/mainstyle.css">
    <link rel="stylesheet" type="text/css" href="http://<?= $_SERVER['HTTP_HOST'] ?>/public/css/fontawesome/css/fontawesome.css">
    <link rel="stylesheet" type="text/css" href="http://<?= $_SERVER['HTTP_HOST'] ?>/public/css/fontawesome/css/solid.css">

    <script type="text/javascript" src="http://<?= $_SERVER['HTTP_HOST'] ?>/public/js/prv.js"></script>
    <title></title>
</head>
<body>

<?php require_once('header_int.php'); ?>
<div class="menu"><?php require_once('menu.php'); ?></div>
<div class="user-data">
    <i class="fas fa-user"></i> <?= $authUser['username'] ?>
</div>
<div class="left-menu"><?php require_once('left_menu.php'); ?></div>
<div class="container">
    <?php if(!empty($messages)): ?>
    <div id="messages">
        <?php foreach ($messages as $message): ?>
                <div class="alert">
                    <span class="mb-0 flex-1"><?= $message ?></span>
                        <img class="btn-message" onclick="hide('messages')" width="15px" src="http://<?= $_SERVER['HTTP_HOST'] ?>/public/img/cross.png">


                </div>
        <?php endforeach; ?>
    </div>
    <?php endif; ?>


    <?php require_once($viewName.'.php'); ?>
</div>
<div class="footer"><?php require_once('footer.php'); ?></div>
</body>
</html>
<script>
     function hide(element) {
        var e = document.getElementById(element);
         if (typeof(e) != 'undefined' && e != null) {
             e.style.display = "none";
         }
    }

     window.setTimeout('hide("messages")', 5000);
</script>