<header class="logo">
    <img src="http://<?= $_SERVER['HTTP_HOST'] ?>/public/img/logo.png" height="100%">
    <span class="float-right logout">
        <a href="http://<?= $_SERVER['HTTP_HOST'] ?>/auth/logout" onclick="return confirm('Are you sure?')"><i class="fas fa-door-open"></i> Logout</a>
    </span>
</header>