<div>
    <span>
        <a title="Add deck" href="/user_decks/add"><i class="fas fa-plus-circle"></i></a>
    </span>
</div>
<table class="table">
    <thead>
    <tr class="thead">
        <th>Deckname</th>
        <th>Colors</th>
        <th class="text-right">###</th>
    </tr>
    </thead>
    <tbody>
    <?php foreach ($user_decks as $user_deck): ?>
        <tr>
            <td><?= $user_deck->getDeckname() ?>
            <?php if($my_decks): ?>
                <a class="float-right" href="edit/<?= $user_deck->getId() ?>"><? require 'buttons/edit.php' ?></a>
            <?php endif; ?>
            </td>
            <td >

                <?php if($user_deck->getIsWhite()): ?>
                    <img src="/public/img/white.png" width="20px" alt="">
                <?php endif; ?>
                <?php if($user_deck->getIsRed()): ?>
                    <img src="/public/img/red.png" width="20px" alt="">
                <?php endif; ?>
                <?php if($user_deck->getIsBlack()): ?>
                    <img src="/public/img/black.png" width="20px" alt="">
                <?php endif; ?>
                <?php if($user_deck->getIsGreen()): ?>
                    <img src="/public/img/green.png" width="20px" alt="">
                <?php endif; ?>
                <?php if($user_deck->getIsBlue()): ?>
                    <img src="/public/img/blue.png" width="20px" alt="">
                <?php endif; ?>

            </td>
            <td class="text-right">
                <a href="/deck_cards/list/<?= $user_deck->getId() ?>">

                    <?php
                    if($my_decks) {
                        require 'buttons/edit.php';
                    } else {
                        require 'buttons/preview.php';
                    }
                    ?>
                </a>
                <a href="copy/<?= $user_deck->getId() ?>" onclick="return confirm('Are you sure?')"><? require 'buttons/copy.php' ?></a>
                <a href="delete/<?= $user_deck->getId() ?>" onclick="return confirm('Are you sure?')"><? require 'buttons/delete.php' ?></a>
            </td>
        </tr>
    <?php endforeach; ?>
    </tbody>
</table>




