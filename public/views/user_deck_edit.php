<div>
    <span>
        <a title="My decks" href="/user_decks/"><i class="fas fa-list"></i></a>
    </span>
</div>

<div>
    <form class="" action="/user_decks/<?= $action ?>" method="POST">
        <div class="form-group">
            <label for="deckname">Deckname:</label>
            <?php if($action == 'edit'): ?>
                <input name="id" type="hidden" value="<?= $user_deck->getId() ?>" required>
            <?php  endif; ?>
            <input name="deckname" type="text" value="<?= ($user_deck ? $user_deck->getDeckname() : '') ?>" placeholder="deckname" required>
        </div>
            <button class="button" type="submit">Save</button>
        <a href="/user_decks/"><button class="button" type="button">Cancel</button></a>
    </form>
</div>
