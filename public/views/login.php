<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" type="text/css" href="http://<?= $_SERVER['HTTP_HOST'] ?>/public/css/style.css">
    <title>LOGIN</title>
</head>
<body>
<?php
    require_once('header.php');
?>
    <div class="messages">
        <?php if(isset($messages)) {
            foreach ($messages as $message){
                echo $message;
            }
        }
        ?>
    </div>
    <div class="register">
        <h1>
            Don't have an account?<br>
            Sign up for free!
        </h1>
        <a href="register">
            <button style="min-width: 150px" class="button" type="button">Register</button>
        </a>
    </div>
    <div class="login-form">
        <form class="login" action="login" method="POST">
            <input name="email" type="text" placeholder="email" required>
            <input name="password" type="password" placeholder="password" required>
            <button class="button" type="submit">Login</button>
        </form>
    </div>
</body>
</html>