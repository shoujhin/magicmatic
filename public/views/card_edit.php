<div>
    <span>
        <a title="My decks" href="/cards/"><i class="fas fa-list"></i></a>
    </span>
</div>

<div class="login-form">
    <form class="form" action="<?= $action ?>" method="POST">
        <div class="form-group">
            <label for="cardname">Cardname:</label>
            <?php if($action == 'edit'): ?>
            <input name="id" type="hidden" value="<?= $card->getId() ?>" required>
            <?php  endif; ?>
            <input name="cardname" type="text" placeholder="cardname" value="<?= $card->getCardname() ?>"  required <?= ($action == 'view' ? 'disabled' : '') ?>>
        </div>
        <div class="form-group">
            <label for="price">Price:</label>
            <input name="price" type="number" placeholder="price" value="<?= $card->getPrice() ?>"  required <?= ($action == 'view' ? 'disabled' : '') ?>>
        </div>
        <div class="form-group">
            <label for="set_id">Wybierdz set:</label>

            <select class="select" name="set_id" <?= ($action == 'view' ? 'disabled' : '') ?>>
                <option value="1" <?= ($card->getSetId() == 1 ? 'selected' : '') ?>>1</option>
                <option value="2" <?= ($card->getSetId() == 2 ? 'selected' : '') ?>>2</option>
                <option value="3" <?= ($card->getSetId() == 3 ? 'selected' : '') ?>>3</option>
                <option value="4" <?= ($card->getSetId() == 4 ? 'selected' : '') ?>>4</option>
            </select>
        </div>
        <div class="form-group">
            <input type="hidden" name="is_white" id="is_white" value="<?= ($card->getIsWhite() ? 1 : 0) ?>">
            <input class="land" type="checkbox" name="is_white" id="is_white_" onclick="setValue('is_white')" value="1" <?= ($card->getIsWhite() ? 'checked' : '') ?> <?= ($action == 'view' ? 'disabled' : '') ?>>
            <label for="is_white"><img src="/public/img/white.png" width="20px" alt=""></label>
        </div>
        <div class="form-group">
            <input type="hidden" name="is_red" id="is_red" value="<?= ($card->getIsRed() ? 1 : 0) ?>">
            <input class="land" type="checkbox" name="is_red" id="is_red_" onclick="setValue('is_red')" value="1" <?= ($card->getIsRed() ? 'checked' : '') ?> <?= ($action == 'view' ? 'disabled' : '') ?>>
            <label for="is_red"><img src="/public/img/red.png" width="20px" alt=""></label>
        </div>
        <div class="form-group">
            <input type="hidden" name="is_black" id="is_black" value="<?= ($card->getIsBlack() ? 1 : 0) ?>">
            <input class="land" type="checkbox" name="is_black" id="is_black_" onclick="setValue('is_black')" value="1" <?= ($card->getIsBlack() ? 'checked' : '') ?> <?= ($action == 'view' ? 'disabled' : '') ?>>
            <label for="is_black"><img src="/public/img/black.png" width="20px" alt=""></label>
        </div>
        <div class="form-group">
            <input type="hidden" id="is_green" name="is_green" value="<?= ($card->getIsGreen() ? 1 : 0) ?>">
            <input class="land" type="checkbox" name="is_green" id="is_green_" onclick="setValue('is_green')" value="1" <?= ($card->getIsGreen() ? 'checked' : '') ?> <?= ($action == 'view' ? 'disabled' : '') ?>>
            <label for="is_green"><img src="/public/img/green.png" width="20px" alt=""></label>
        </div>
        <div class="form-group">
            <input type="hidden" name="is_blue" id="is_blue" value="<?= ($card->getIsBlue() ? 1 : 0) ?>">
            <input class="land" type="checkbox" name="is_blue"  id="is_blue_" onclick="setValue('is_blue')" value="1" <?= ($card->getIsBlue() ? 'checked' : '') ?> <?= ($action == 'view' ? 'disabled' : '') ?>>
            <label for="is_blue"><img src="/public/img/blue.png" width="20px" alt=""></label>
        </div>
        <?php if($action != 'view'): ?>
        <button class="button" type="submit">Save</button>
        <?php  endif; ?>
        <a href="/cards/"><button class="button" type="button">Cancel</button></a>

    </form>
</div>
<script>
    function setValue(param) {
         console.log(param);
        var a = document.getElementById(param+"_").checked;
        if(a) {
            document.getElementById(param).value = 1;
        } else {
            document.getElementById(param).value = 0;
        }
         console.log(a);
    }
</script>