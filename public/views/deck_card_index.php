
<h2>
    <?= $user_deck->getDeckname(); ?>
    <?php if($user_deck->getIsWhite()): ?>
        <img src="/public/img/white.png" width="20px" alt="">
    <?php endif; ?>
    <?php if($user_deck->getIsRed()): ?>
        <img src="/public/img/red.png" width="20px" alt="">
    <?php endif; ?>
    <?php if($user_deck->getIsBlack()): ?>
        <img src="/public/img/black.png" width="20px" alt="">
    <?php endif; ?>
    <?php if($user_deck->getIsGreen()): ?>
        <img src="/public/img/green.png" width="20px" alt="">
    <?php endif; ?>
    <?php if($user_deck->getIsBlue()): ?>
        <img src="/public/img/blue.png" width="20px" alt="">
    <?php endif; ?>
</h2>
<?php if($is_owner): ?>
<div class="login-form">
    <form class="form" action="/deck_cards/add" method="POST">
        <div class="form-group">
                <input name="deck_id" type="hidden" value="<?= $user_deck->getId() ?>" required>

            <label for="card_id">Choose a card:</label>

            <select class="select" name="card_id" >
                <?php foreach ($cards_for_deck as $card): ?>
                <option value="<?= $card->getId() ?>" ><?= $card->getCardname() ?></option>
                <?php endforeach; ?>
            </select>
        </div>
        <div class="form-group">
            <label for="price">Quantity</label>
            <input name="quantity" type="number" placeholder="quantity"  required>
        </div>

            <button class="button" type="submit">Save</button>
        <a href="/cards/"><button class="button" type="button">Cancel</button></a>

    </form>
</div>
<hr>
<?php endif; ?>
<table class="table">
    <thead>


    <tr class="thead">
        <th>Cardname</th>
        <th>Quantity</th>
        <th>Price</th>
        <th>Colors</th>
        <th class="text-right">###</th>
    </tr>
    </thead>
    <tbody>
    <?php $total_price = 0; ?>
    <?php foreach ($deck_cards as $deck_card): ?>
        <?php $total_price += $deck_card->getCard()->getPrice()*$deck_card->getQuantity(); ?>
        <tr>
            <td><?= $deck_card->getCard()->getCardname() ?></td>
            <td><?= $deck_card->getQuantity() ?></td>
            <td class="text-right"><?= $deck_card->getCard()->getPrice() ?></td>
            <td >

                <?php if($deck_card->getCard()->getIsWhite()): ?>
                    <img src="/public/img/white.png" width="20px" alt="">
                <?php endif; ?>
                <?php if($deck_card->getCard()->getIsRed()): ?>
                    <img src="/public/img/red.png" width="20px" alt="">
                <?php endif; ?>
                <?php if($deck_card->getCard()->getIsBlack()): ?>
                    <img src="/public/img/black.png" width="20px" alt="">
                <?php endif; ?>
                <?php if($deck_card->getCard()->getIsGreen()): ?>
                    <img src="/public/img/green.png" width="20px" alt="">
                <?php endif; ?>
                <?php if($deck_card->getCard()->getIsBlue()): ?>
                    <img src="/public/img/blue.png" width="20px" alt="">
                <?php endif; ?>

            </td>
            <td class="text-right">
                <a href="/deck_cards/delete/<?= $deck_card->getId() ?>" onclick="return confirm('Are you sure?')"><? require 'buttons/delete.php' ?></a>
            </td>
        </tr>
    <?php endforeach; ?>
    </tbody>
    <tfooter>
        <tr>
            <td></td>
            <td></td>
            <td class="text-right text-bold"><?= $total_price  ?></td>
            <td></td>
            <td></td>
        </tr>
    </tfooter>
</table>




