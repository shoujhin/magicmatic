<div>
    <span>
        <a title="Add card" href="add"><i class="fas fa-plus-circle"></i></a>
    </span>
</div>


<table class="table">
    <thead>


    <tr class="thead">
        <th>Cardname</th>
        <th>Set</th>
        <th>Price</th>
        <th>Colors</th>
        <th class="text-right">###</th>
    </tr>
    </thead>
    <tbody>
    <?php foreach ($cards as $card): ?>
        <tr>
            <td><?= $card->getCardname() ?></td>
            <td><?= $card->getSetId() ?></td>
            <td class="text-right"><?= $card->getPrice() ?></td>
            <td >

                <?php if($card->getIsWhite()): ?>
                    <img src="/public/img/white.png" width="20px" alt="">
                <?php endif; ?>
                <?php if($card->getIsRed()): ?>
                    <img src="/public/img/red.png" width="20px" alt="">
                <?php endif; ?>
                <?php if($card->getIsBlack()): ?>
                    <img src="/public/img/black.png" width="20px" alt="">
                <?php endif; ?>
                <?php if($card->getIsGreen()): ?>
                    <img src="/public/img/green.png" width="20px" alt="">
                <?php endif; ?>
                <?php if($card->getIsBlue()): ?>
                    <img src="/public/img/blue.png" width="20px" alt="">
                <?php endif; ?>

            </td>
            <td class="text-right">
                <a href="view/<?= $card->getId() ?>"><? require 'buttons/preview.php' ?></a>
                <a href="edit/<?= $card->getId() ?>"><? require 'buttons/edit.php' ?></a>
                <a href="delete/<?= $card->getId() ?>" onclick="return confirm('Are you sure?')"><? require 'buttons/delete.php' ?></a>
            </td>
        </tr>
    <?php endforeach; ?>
    </tbody>
</table>




