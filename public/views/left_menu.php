<ul>
    <?php if($authUser['role_id'] == 1): ?>
    <li>
        <a href="http://<?= $_SERVER['HTTP_HOST'] ?>/users/"><i class="fas fa-users"></i> Users</a>
    </li>
    <?php endif; ?>
    <li>
        <a href="http://<?= $_SERVER['HTTP_HOST'] ?>/cards/"><i class="fas fa-layer-group"></i> Cards</a>
    </li>
    <li>
        <a href="http://<?= $_SERVER['HTTP_HOST'] ?>/user_decks/"><i class="fas fa-dice-d6"></i> My Decks</a>
    </li>
    <li>
        <a href="http://<?= $_SERVER['HTTP_HOST'] ?>/user_decks/all"><i class="fas fa-cubes"></i> All Decks</a>
    </li>
    <li></li>
    <li></li>
</ul>