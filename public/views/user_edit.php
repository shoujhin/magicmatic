<div>
    <span>
        <a title="User list" href="/users/"><i class="fas fa-list"></i></a>
    </span>
</div>

<div>
    <form class="" action="<?= $action ?>" method="POST">
        <div class="form-group">
            <label for="username">Username:</label>
            <?php if($action == 'edit'): ?>
                <input name="id" type="hidden" value="<?= $user->getId() ?>" required>
            <?php  endif; ?>
            <input name="username" type="text" placeholder="username" value="<?= $user->getUsername() ?>" required <?= ($action == 'view' ? 'disabled' : '') ?>>
        </div>
        <div class="form-group">
            <label for="email">Email:</label>
            <input name="email" type="text" placeholder="email" value="<?= $user->getEmail() ?>" required <?= ($action == 'view' ? 'disabled' : '') ?>>
        </div>
        <?php if($action == 'add'): ?>
        <div class="form-group">
            <label for="password">Password:</label>
            <input name="password" type="password" placeholder="password" required>
        </div>

        <div class="form-group">
            <label for="password_">Repeat password:</label>
            <input name="password_" type="password" placeholder="password" required>
        </div>
        <?php  endif; ?>

        <div class="form-group">
            <label for="role_id">Role_id:</label>

            <select class="select" name="role_id" <?= ($action == 'view' ? 'disabled' : '') ?>>
                <option value="1" <?= ($user->getRoleId() == 1 ? 'selected' : '') ?>>Admin</option>
                <option value="2" <?= ($user->getRoleId() == 2 ? 'selected' : '') ?>>User</option>
            </select>
        </div>
        <?php if($action != 'view'): ?>
            <button class="button" type="submit">Save</button>
        <?php  endif; ?>
        <a href="/users/"><button class="button" type="button">Cancel</button></a>
    </form>
</div>
