<div>
    <span>
        <a title="Add user" href="add"><i class="fas fa-user-plus"></i></a>
    </span>
</div>

<table class="table">
    <thead>
    <tr class="thead">
        <th>username</th>
        <th>email</th>
        <th>role</th>
        <th class="text-right">###</th>
    </tr>
    </thead>
    <tbody>
    <?php foreach ($users as $user): ?>
        <tr>
            <td><?= $user->getUsername() ?></td>
            <td><?= $user->getEmail() ?></td>
            <td><?= $user->getRole()->getRolename() ?></td>
            <td class="text-right">
                <a href="view/<?= $user->getId() ?>"><? require 'buttons/preview.php' ?></a>
                <a href="edit/<?= $user->getId() ?>"><? require 'buttons/edit.php' ?></a>
                <a href="delete/<?= $user->getId() ?>" onclick="return confirm('Are you sure?')"><? require 'buttons/delete.php' ?></a>
            </td>
        </tr>
    <?php endforeach; ?>
    </tbody>
</table>




