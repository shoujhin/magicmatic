<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" type="text/css" href="http://<?= $_SERVER['HTTP_HOST'] ?>/public/css/style.css">
    <title>REGISTER</title>
</head>
<body>
<?php
    require_once('header.php');
?>
<div class="messages">
    <?php if(isset($messages)) {
        foreach ($messages as $message){
            echo $message;
        }
    }
    ?>
</div>
<div class="register">
    <h1>
        Already have an account?<br>
        Log in!
    </h1>
    <a href="login">
        <button style="min-width: 150px" class="button" type="button">Login</button>
    </a>
</div>
<div class="login-form">
    <form class="login" action="register" method="POST">
        <input name="email" type="text" placeholder="email">
        <input name="username" type="text" placeholder="username">
        <input name="password" type="password" placeholder="password">
        <button class="button" type="submit">Register</button>
    </form>
</div>
</body>
</html>