<?php

require_once __DIR__.'//..//models//Card.php';
require_once "Repository.php";

class CardsRepository extends Repository
{
    public function getCards(): array
    {
        $stmt = $this->database->connect()->prepare('
            SELECT * FROM cards 
        ');

        $stmt->execute();

        $cards = $stmt->fetchAll(PDO::FETCH_ASSOC);

        $result = [];
        foreach ($cards as $card) {
            $result[] = new Card(
                $card['cardname'],
                $card['set_id'],
                $card['is_white'],
                $card['is_red'],
                $card['is_black'],
                $card['is_green'],
                $card['is_blue'],
                $card['price'],
                $card['id']
            );
        }

        return $result;
    }

    public function getCard($id): ?Card
    {

        if($id == 0) {
            return new Card(
                '',
                1,
                0,
                0,
                0,
                0,
                0,
                0
            );
        }
        $stmt = $this->database->connect()->prepare('
            SELECT * FROM cards WHERE id = :id
        ');
        $stmt->bindParam(':id', $id, PDO::PARAM_INT);
        $stmt->execute();

        $card = $stmt->fetch(PDO::FETCH_ASSOC);
        return new Card(
            $card['cardname'],
            $card['set_id'],
            $card['is_white'],
            $card['is_red'],
            $card['is_black'],
            $card['is_green'],
            $card['is_blue'],
            $card['price'],
            $card['id']
        );
    }

    public function addCard(Card $card): int
    {
        $stmt = $this->database->connect()->prepare('
            INSERT INTO cards (cardname, set_id, is_white, is_red, is_black, is_green, is_blue, price)
            VALUES (?, ?, ?, ?, ?, ?, ?, ?) RETURNING id
        ');
        $stmt->execute([
            $card->getCardname(),
            $card->getSetId(),
            $card->getIsWhite(),
            $card->getIsRed(),
            $card->getIsBlack(),
            $card->getIsGreen(),
            $card->getIsBlue(),
            $card->getPrice()

        ]);
        $card = $stmt->fetch(PDO::FETCH_ASSOC);
        return $card['id'];
    }

    public function beforeDeleteCard($id): bool
    {
        $stmt = $this->database->connect()->prepare('
            SELECT count(*) AS qty from decks_cards WHERE card_id = :id
        ');
        $stmt->bindParam(':id', $id, PDO::PARAM_INT);
        $stmt->execute();
        $card = $stmt->fetch(PDO::FETCH_ASSOC);
        if($card['qty'] == 0) {
            return false;
        }
        return true;
    }

    public function deleteCard($id): void
    {
        $stmt = $this->database->connect()->prepare('
            DELETE FROM cards WHERE id = :id
        ');
        $stmt->bindParam(':id', $id, PDO::PARAM_INT);
        $stmt->execute();
    }

    public function editCard($card): void
    {
        $stmt = $this->database->connect()->prepare('
            UPDATE cards  SET 
                cardname = :cardname, 
                set_id = :set_id, 
                is_white = :is_white,
                is_red = :is_red, 
                is_black = :is_black, 
                is_green = :is_green, 
                is_blue = :is_blue,
                price = :price
            WHERE id = :id
        ');

        $stmt->bindParam(':cardname', $card['cardname'], PDO::PARAM_STR);
        $stmt->bindParam(':set_id', $card['set_id'], PDO::PARAM_INT);
        $stmt->bindParam(':is_white', $card['is_white'], PDO::PARAM_BOOL);
        $stmt->bindParam(':is_red', $card['is_red'], PDO::PARAM_BOOL);
        $stmt->bindParam(':is_black', $card['is_black'], PDO::PARAM_BOOL);
        $stmt->bindParam(':is_green', $card['is_green'], PDO::PARAM_BOOL);
        $stmt->bindParam(':is_blue', $card['is_blue'], PDO::PARAM_BOOL);
        $stmt->bindParam(':price', $card['price'], PDO::PARAM_STR);
        $stmt->bindParam(':id', $card['id'], PDO::PARAM_INT);

        $stmt->execute();
    }
}




