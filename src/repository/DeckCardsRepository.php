<?php

require_once "Repository.php";
require_once __DIR__.'//..//models//Card.php';
require_once __DIR__.'//..//models//DeckCard.php';


class DeckCardsRepository extends Repository
{
    public function getDeckCards($deckId): array {
        $stmt = $this->database->connect()->prepare('
            SELECT d.id, d.deck_id, d.card_id, d.quantity, 
            c.cardname, c.set_id, c.is_white, c.is_red, c.is_black, c.is_green, c.is_blue, c.price   FROM decks_cards d 
            left join cards c on d.card_id = c.id 
            WHERE d.deck_id = :deckId;
        ');

        $stmt->bindParam(':deckId', $deckId, PDO::PARAM_INT);
        $stmt->execute();
        $deck_cards = $stmt->fetchAll(PDO::FETCH_ASSOC);

        $result = [];
        foreach ($deck_cards as $deck_card) {
            $currDeck = new DeckCard(
                $deck_card['deck_id'],
                $deck_card['card_id'],
                $deck_card['quantity'],
                $deck_card['id']
            );
            $currDeck->setCard(
                $deck_card['cardname'],
                $deck_card['set_id'],
                $deck_card['is_white'],
                $deck_card['is_red'],
                $deck_card['is_black'],
                $deck_card['is_green'],
                $deck_card['is_blue'],
                $deck_card['price'],
                $deck_card['card_id']
            );

            $result[]=$currDeck;
        }
        return $result;
    }

    public function getDeckCard($Id): ?DeckCard {
        $stmt = $this->database->connect()->prepare('
            SELECT d.id, d.deck_id, d.card_id, d.quantity, 
            c.cardname, c.set_id, c.is_white, c.is_red, c.is_black, c.is_green, c.is_blue, c.price      
            FROM decks_cards d 
            left join cards c on d.card_id = c.id 
            WHERE d.id = :Id;
        ');

        $stmt->bindParam(':Id', $Id, PDO::PARAM_INT);
        $stmt->execute();
        $deck_card = $stmt->fetch(PDO::FETCH_ASSOC);

            $currDeckCard= new DeckCard(
                $deck_card['deck_id'],
                $deck_card['card_id'],
                $deck_card['quantity'],
                $deck_card['id']
            );
            $currDeckCard->setCard(
                $deck_card['cardname'],
                $deck_card['set_id'],
                $deck_card['is_white'],
                $deck_card['is_red'],
                $deck_card['is_black'],
                $deck_card['is_green'],
                $deck_card['is_blue'],
                $deck_card['price'],
                $deck_card['card_id']
            );


        return $currDeckCard;
    }

    public function deleteDeckCard($id): void
    {
        $stmt = $this->database->connect()->prepare('
            DELETE FROM decks_cards WHERE id = :id
        ');
        $stmt->bindParam(':id', $id, PDO::PARAM_INT);
        $stmt->execute();
    }

    public function getCardsForDeck($id): array
    {
        $stmt = $this->database->connect()->prepare('
            SELECT * FROM cards 
            WHERE id NOT IN (
                SELECT card_id FROM decks_cards WHERE deck_id = :id
            );
        ');

        $stmt->bindParam(':id', $id, PDO::PARAM_INT);
        $stmt->execute();
        $deck_cards = $stmt->fetchAll(PDO::FETCH_ASSOC);
        $result = [];
        foreach ($deck_cards as $deck_card) {
            $result[] = new Card(
                $deck_card['cardname'],
                $deck_card['set_id'],
                $deck_card['is_white'],
                $deck_card['is_red'],
                $deck_card['is_black'],
                $deck_card['is_green'],
                $deck_card['is_blue'],
                $deck_card['price'],
                $deck_card['id']
            );
        }
        return $result;
    }

    public function addDeckCard(DeckCard $deckCard): int
    {
        $stmt = $this->database->connect()->prepare('
            INSERT INTO decks_cards (deck_id, card_id, quantity)
            VALUES (?, ?, ?) RETURNING id
        ');
        $stmt->execute([
            $deckCard->getDeckId(),
            $deckCard->getCardId(),
            $deckCard->getQuantity(),
        ]);
        $result = $stmt->fetch(PDO::FETCH_ASSOC);
        return $result['id'];
    }

}