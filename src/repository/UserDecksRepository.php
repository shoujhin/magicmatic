<?php

require_once "Repository.php";
require_once __DIR__.'//..//models//UserDeck.php';


class UserDecksRepository extends Repository
{
    public function getUserDecks($userId=NULL): array
    {
        $stmt = $this->database->connect()->prepare('
            SELECT * FROM users_decks 
        ');

        if(!empty($userId)) {
            $stmt = $this->database->connect()->prepare('
                SELECT * FROM users_decks 
                WHERE user_id = :userId
            ');
            $stmt->bindParam(':userId', $userId, PDO::PARAM_INT);
        }
        $stmt->execute();

        $user_decks = $stmt->fetchAll(PDO::FETCH_ASSOC);
        $result = [];
        foreach ($user_decks as $user_deck) {
            $result[] = new UserDeck(
                $user_deck['deckname'],
                $user_deck['user_id'],
                $user_deck['id'],
                $user_deck['is_white'],
                $user_deck['is_red'],
                $user_deck['is_black'],
                $user_deck['is_green'],
                $user_deck['is_blue'],
                $user_deck['creation_date']

            );
        }

        return $result;
    }

    public function getUserDeck($deckId): ?UserDeck
    {
        $stmt = $this->database->connect()->prepare('
            SELECT * FROM users_decks 
            WHERE id = :deckId
        ');

        $stmt->bindParam(':deckId', $deckId, PDO::PARAM_INT);

        $stmt->execute();

        $user_deck = $stmt->fetch(PDO::FETCH_ASSOC);
        return new UserDeck(
            $user_deck['deckname'],
            $user_deck['user_id'],
            $user_deck['id'],
            $user_deck['is_white'],
            $user_deck['is_red'],
            $user_deck['is_black'],
            $user_deck['is_green'],
            $user_deck['is_blue'],
            $user_deck['creation_date']
        );

    }

    public function deleteUserDeck($deckId): void
    {
        $stmt = $this->database->connect()->prepare('
            DELETE FROM users_decks WHERE id = :id
        ');
        $stmt->bindParam(':id', $deckId, PDO::PARAM_INT);
        $stmt->execute();
    }

    public function addUserDeck(UserDeck $userDeck): int
    {
        $stmt = $this->database->connect()->prepare('
            INSERT INTO users_decks (deckname, user_id)
            VALUES (?, ?) RETURNING id
        ');
        $stmt->execute([
            $userDeck->getDeckname(),
            $userDeck->getUserId(),

        ]);
        $deck = $stmt->fetch(PDO::FETCH_ASSOC);
        return $deck['id'];
    }

    public function copyDeck($fromDeckId, $toDeckId): void
    {
        $stmt = $this->database->connect()->prepare('
            INSERT INTO decks_cards (deck_id, card_id, quantity) 
                SELECT
                    '.$toDeckId.', card_id, quantity 
                FROM decks_cards
                WHERE deck_id = :fromDeckId 
        ');
        $stmt->bindParam(':fromDeckId', $fromDeckId, PDO::PARAM_INT);
        $stmt->execute();
    }

    public function editUserDeck(UserDeck $userDeck): void
    {
        $stmt = $this->database->connect()->prepare('
            UPDATE users_decks 
            SET 
                deckname = :deckname
            WHERE id = :id
        ');
        $deckname = $userDeck->getDeckname();
        $id = $userDeck->getId();

        $stmt->bindParam(':deckname', $deckname, PDO::PARAM_STR);
        $stmt->bindParam(':id', $id, PDO::PARAM_INT);

        $stmt->execute();
    }
}