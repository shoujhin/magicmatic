<?php

require_once "Repository.php";
require_once __DIR__.'//..//models//User.php';

class UsersRepository extends Repository {

    public function getUser(int $id): ?User
    {
        if($id == 0) {
            return new User(
                '',
                '',
                2
            );
        }


        $stmt = $this->database->connect()->prepare('
            SELECT * FROM users WHERE id = :id
        ');
        $stmt->bindParam(':id', $id, PDO::PARAM_INT);
        $stmt->execute();

        $user = $stmt->fetch(PDO::FETCH_ASSOC);

        if($user == false) {
            return null;
        }

        return new User(
            $user['email'],
            $user['username'],
            $user['role_id'],
            $user['id']
        );
    }

    public function getUserLogin(string $email): ?User
    {

        $stmt = $this->database->connect()->prepare('
            SELECT * FROM users WHERE (email = :email)
        ');
        $stmt->bindParam(':email', $email, PDO::PARAM_STR);
        $stmt->execute();

        $user = $stmt->fetch(PDO::FETCH_ASSOC);

        if($user == false) {
            return null;
        }

        return new User(
            $user['email'],
            $user['username'],
            $user['role_id'],
            $user['id'],
            $user['password']
        );
    }

    public function getUsers(): array {
        $stmt = $this->database->connect()->prepare('
            SELECT u.id , u.username, u.role_id, u.email, r.rolename FROM users u 
            left join roles r on r.id = u.role_id;
        ');

        $stmt->execute();
        $users = $stmt->fetchAll(PDO::FETCH_ASSOC);

        $result = [];
         foreach ($users as $user) {
             $userCurr = new User(
                 $user['email'],
                 $user['username'],
                 $user['role_id'],
                 $user['id']
             );
             $userCurr->setRole($user['role_id'], $user['rolename']);

             $result[]=$userCurr;
         }

        return $result;
    }

    public function addUser(User $user): int
    {
        $stmt = $this->database->connect()->prepare('
            INSERT INTO users (email, username, role_id, password)
            VALUES (?, ?, ?, ?) RETURNING id
        ');
        $stmt->execute([
            $user->getEmail(),
            $user->getUsername(),
            $user->getRoleId(),
            md5($user->getPassword())
        ]);
        $user = $stmt->fetch(PDO::FETCH_ASSOC);
        return $user['id'];
    }

    public function deleteUser($id): void
    {
        $stmt = $this->database->connect()->prepare('
            DELETE FROM users WHERE id = :id
        ');
        $stmt->bindParam(':id', $id, PDO::PARAM_INT);
        $stmt->execute();
    }

    public function editUser($user): void
    {
        $stmt = $this->database->connect()->prepare('
            UPDATE users  SET 
                username = :username, 
                email = :email, 
                role_id = :role_id
            WHERE id = :id
        ');

        $stmt->bindParam(':username', $user['username'], PDO::PARAM_STR);
        $stmt->bindParam(':email', $user['email'], PDO::PARAM_STR);
        $stmt->bindParam(':role_id', $user['role_id'], PDO::PARAM_INT);
        $stmt->bindParam(':id', $user['id'], PDO::PARAM_INT);

        $stmt->execute();
    }
}