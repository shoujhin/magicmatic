<?php

class Role
{
    private $id;
    private $rolename;

    public function __construct(int $id, string $rolename)
    {
        $this->id = $id;
        $this->rolename = $rolename;
    }

    public function getId()
    {
        return $this->id;
    }

    public function setId($id): void
    {
        $this->id = $id;
    }

    public function getRolename()
    {
        return $this->rolename;
    }

    public function setRolename($rolename): void
    {
        $this->rolename = $rolename;
    }


}