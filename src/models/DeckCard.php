<?php

class DeckCard
{
    private $id;
    private $deck_id;
    private $card_id;
    private $quantity;
    private $card;

    public function __construct(int $deck_id, int $card_id, int $quantity,int $id=NULL)
    {
        $this->id = $id;
        $this->deck_id = $deck_id;
        $this->card_id = $card_id;
        $this->quantity = $quantity;
    }


    public function getCard(): ?Card
    {
        return $this->card;
    }

    public function setCard(string $cardname,int $set_id,int $is_white,int $is_red,int $is_black,int $is_green,int $is_blue, float $price,int $id = NULL): void
    {
        $this->card = new Card(
            $cardname
            ,$set_id
            ,$is_white
            ,$is_red
            ,$is_black
            ,$is_green
            ,$is_blue
            ,$price
            ,$id
        );
    }



    public function getId(): ?int
    {
        return $this->id;
    }

    public function setId(?int $id): void
    {
        $this->id = $id;
    }

    public function getDeckId(): int
    {
        return $this->deck_id;
    }

    public function setDeckId(int $deck_id): void
    {
        $this->deck_id = $deck_id;
    }

    public function getCardId(): int
    {
        return $this->card_id;
    }

    public function setCardId(int $card_id): void
    {
        $this->card_id = $card_id;
    }

    public function getQuantity(): int
    {
        return $this->quantity;
    }

    public function setQuantity(int $quantity): void
    {
        $this->quantity = $quantity;
    }

}