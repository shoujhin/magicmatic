<?php

require_once 'Role.php';

class User
{
    private $id;
    private $email;
    private $password;
    private $username;
    private $role_id;
    private $role;

    public function getRole(): ?Role
    {
        return $this->role;
    }

    public function setRole($id, $rolename): void
    {
        $this->role = new Role($id, $rolename);
    }

    public function __construct(string $email, string $username, int $role_id, int $id=NULL, string $password=NULL)
    {
        $this->email = $email;
        $this->password = $password;
        $this->username = $username;
        $this->id = $id;
        $this->role_id = $role_id;
    }

    public function getId(): int
    {
        return $this->id;
    }

    public function setId($id): void
    {
        $this->id = $id;
    }

    public function getEmail(): string
    {
        return $this->email;
    }

    public function setEmail(string $email): void
    {
        $this->email = $email;
    }

    public function getPassword(): string
    {
        return $this->password;
    }

    public function setPassword(string $password): void
    {
        $this->password = $password;
    }

    public function getUsername(): string
    {
        return $this->username;
    }

    public function setUsername(string $username): void
    {
        $this->username = $username;
    }

    public function getRoleId(): int
    {
        return $this->role_id;
    }

    public function setRoleId(int $role_id): void
    {
        $this->role_id = $role_id;
    }


}