<?php

require_once __DIR__.'//..//..//Database.php';


class Card
{
    private $id;
    private $cardname;
    private $set_id;
    private $is_white;
    private $is_red;
    private $is_black;
    private $is_green;
    private $is_blue;
    private $price;

    public function __construct(string $cardname,int $set_id,int $is_white,int $is_red,int $is_black,int $is_green,int $is_blue, float $price,int $id = NULL)
    {

        $this->cardname = $cardname;
        $this->set_id = $set_id;
        $this->is_white = $is_white;
        $this->is_red = $is_red;
        $this->is_black = $is_black;
        $this->is_green = $is_green;
        $this->is_blue = $is_blue;
        $this->price = $price;
        $this->id =$id;
    }


    public function getPrice(): float
    {
        return $this->price;
    }

    public function setPrice(float $price): void
    {
        $this->price = $price;
    }



    public function getId(): int
    {
        return $this->id;
    }


    public function setId($id): void
    {
        $this->id = $id;
    }

    public function getCardname(): string
    {
        return $this->cardname;
    }


    public function setCardname($cardname): void
    {
        $this->cardname = $cardname;
    }


    public function getSetId(): int
    {
        return $this->set_id;
    }

    public function setSetId($set_id): void
    {
        $this->set_id = $set_id;
    }

    public function getIsWhite(): int
    {
        return $this->is_white;
    }

    public function setIsWhite($is_white): void
    {
        $this->is_white = $is_white;
    }

    public function getIsRed()
    {
        return $this->is_red;
    }

    public function setIsRed($is_red)
    {
        $this->is_red = $is_red;
    }

    public function getIsBlack(): int
    {
        return $this->is_black;
    }

    public function setIsBlack($is_black): void
    {
        $this->is_black = $is_black;
    }


    public function getIsGreen(): int
    {
        return $this->is_green;
    }

    public function setIsGreen($is_green): void
    {
        $this->is_green = $is_green;
    }

    public function getIsBlue(): int
    {
        return $this->is_blue;
    }

    public function setIsBlue($is_blue): void
    {
        $this->is_blue = $is_blue;
    }


}