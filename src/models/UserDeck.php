<?php

class UserDeck
{
    private $id;
    private $user_id;
    private $deckname;
    private $is_white;
    private $is_red;
    private $is_black;
    private $is_green;
    private $is_blue;
    private $creation_date;


    public function __construct(string $deckname, int $user_id, int $id=NULL, int $is_white=NULL, int $is_red=NULL, int $is_black=NULL, int $is_green=NULL, int $is_blue=NULL, string $creation_date=NULL)
    {
        $this->id = $id;
        $this->deckname = $deckname;
        $this->user_id = $user_id;
        $this->is_white = $is_white;
        $this->is_red = $is_red;
        $this->is_black = $is_black;
        $this->is_green = $is_green;
        $this->is_blue = $is_blue;
        $this->creation_date = $creation_date;
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function setId(?int $id): void
    {
        $this->id = $id;
    }

    public function getUserId(): ?int
    {
        return $this->user_id;
    }

    public function setUserId(?int $user_id): void
    {
        $this->user_id = $user_id;
    }

    public function getDeckname(): string
    {
        return $this->deckname;
    }

    public function setDeckname(string $deckname): void
    {
        $this->deckname = $deckname;
    }

    public function getIsWhite(): ?int
    {
        return $this->is_white;
    }

    public function setIsWhite(?int $is_white): void
    {
        $this->is_white = $is_white;
    }


    public function getIsRed(): ?int
    {
        return $this->is_red;
    }

    public function setIsRed(?int $is_red): void
    {
        $this->is_red = $is_red;
    }

    public function getIsBlack(): ?int
    {
        return $this->is_black;
    }

    public function setIsBlack(?int $is_black): void
    {
        $this->is_black = $is_black;
    }

    public function getIsGreen(): ?int
    {
        return $this->is_green;
    }

    public function setIsGreen(?int $is_green): void
    {
        $this->is_green = $is_green;
    }

    public function getIsBlue(): ?int
    {
        return $this->is_blue;
    }

    public function setIsBlue(?int $is_blue): void
    {
        $this->is_blue = $is_blue;
    }

    public function getCreationDate(): ?string
    {
        return $this->creation_date;
    }

    public function setCreationDate(?string $creation_date): void
    {
        $this->creation_date = $creation_date;
    }

}