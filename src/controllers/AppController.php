<?php

require_once __DIR__.'//..//..//Database.php';

class AppController {

    private $request;
    private $authUser;
    private $messages;

    public function __construct() {
        $this->request = $_SERVER['REQUEST_METHOD'];
        $this->authUser = $_SESSION['auth'];
        $this->messages=[];
    }

    public function getMessages(): array
    {
        $this->messages = $_SESSION['messages'];
        if (!empty($this->messages)) {
            unset($_SESSION['messages']);
        } else {
            $this->messages = [];
        }
        return $this->messages;
    }

    public function setMessages($message): void
    {
        $m = $_SESSION['messages'];
        $m[] = $message;
        $_SESSION['messages'] = $m;
    }

    protected function getAuthUser() {
        return $this->authUser;
    }

    protected function getAuthUserRoleId() {
        return $this->authUser['role_id'];
    }

    protected function getAuthUserId() {
        return $this->authUser['id'];
    }


    protected function checkPermission($requiredPermission)
    {
        if(!empty($_SESSION['auth'])) {
            if($requiredPermission == 1) { // wymagany poziom administracyjny
                if($_SESSION['auth']['role_id'] != $requiredPermission) {
                    header("Location: http://".$_SERVER['HTTP_HOST']."/auth/nopermissions");
                    exit;
                }
            }
        }
    }

    protected function isPost(): bool {
        return $this->request === 'POST';
    }

    protected function isGet(): bool {
        return $this->request === 'GET';
    }

    protected function render(string $template = null, array $variables = []) {
        $templatePath = 'public/views/'.$template.'.php';
        $output = "File not found";
        if(file_exists($templatePath)) {
            $variables['authUser']=$this->getAuthUser();
            $variables['messages']=$this->getMessages();

            extract($variables);

            ob_start();
            include $templatePath;
            $output = ob_get_clean();
        }
        print $output;  

    }
}