<?php

require_once __DIR__.'//..//repository//DeckCardsRepository.php';
require_once __DIR__.'//..//repository//UserDecksRepository.php';
require_once 'AppController.php';

class DeckCardsController extends AppController
{
    private $deckCardsRepository;

    public function __construct()
    {
        parent::__construct();
        $this->deckCardsRepository = new DeckCardsRepository();
    }

    public function index() {
        echo 'TODO';
        exit;
    }

    public function list($deckId) {
        $userDecks = new UserDecksRepository();
        $isOwner = false;
        $userDeck = $userDecks->getUserDeck($deckId);
        if($userDeck->getUserId() == $this->getAuthUserId()) {
            $isOwner = true;
        }

            $this->render('layout', [
                'deck_cards'=>$this->deckCardsRepository->getDeckCards($deckId),
                'user_deck'=>$userDeck,
                'is_owner'=>$isOwner,
                'cards_for_deck'=>$this->deckCardsRepository->getCardsForDeck($deckId),
                'viewName' =>  'deck_card_index'
        ] );
    }

    public function delete($id) {
        $card = $this->deckCardsRepository->getDeckCard($id);
        $userDecks = new UserDecksRepository();
        $userDecks = $userDecks->getUserDeck($card->getDeckId());
        if($this->getAuthUserId() != $userDecks->getUserId()){
            $this->setMessages('No permission for operation.');
            header('Location: '.'/deck_cards/list/'.$card->getDeckId());
            exit;
        }
        $this->deckCardsRepository->deleteDeckCard($id);
        $this->setMessages('Deck card has been deleted.');
        header('Location: '.'/deck_cards/list/'.$card->getDeckId());
        exit;
    }

    public function add() {
        if($this->isPost()) {
            $deckCard = new DeckCard(
                $_POST['deck_id'],
                $_POST['card_id'],
                $_POST['quantity']
            );

            $userDecks = new UserDecksRepository();
            $userDecks = $userDecks->getUserDeck($deckCard->getDeckId());
            if($this->getAuthUserId() != $userDecks->getUserId()){
                $this->setMessages('No permission for operation.');
                header('Location: '.'/deck_cards/list/'.$deckCard->getDeckId());
                exit;
            }

            $this->deckCardsRepository->addDeckCard($deckCard);
            $this->setMessages('Deck card has been added.');
            header('Location: '.'/deck_cards/list/'.$deckCard->getDeckId());
            exit;
        }

    }
}