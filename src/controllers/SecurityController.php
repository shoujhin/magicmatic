<?php

require_once __DIR__.'//..//repository//UsersRepository.php';
require_once 'AppController.php';

class SecurityController extends AppController
{
    public function index()
    {
        header("Location: /auth/login");
        exit;
    }


    public function login()
    {

        if($this->getAuthUser()) {
            header("Location: /cards/");
            exit;
        }
        if(!$this->isPost()) {
            return $this->render('login');
        }

        $email = $_POST["email"];
        $password = $_POST["password"];
        $userRepository = new UsersRepository();
        $user = $userRepository->getUserLogin($email);

        if(!isset($user)) {
            $this->setMessages('User with this email does not exist.');
            header("Location: ".$_SERVER['HTTP_REFERER']);
            exit;
        }

        if($user->getPassword() !== md5($password)) {
            $this->setMessages('Wrong password');
            header("Location: ".$_SERVER['HTTP_REFERER']);
            exit;
        }

        $authUser = [
            'id' => $user->getId()
            ,'email' => $user->getEmail()
            ,'username' => $user->getUsername()
            ,'role_id' => $user->getRoleId()
        ];
        $_SESSION['auth'] = $authUser;



        header("Location: /cards/");
        exit;

    }

    public function logout()
    {
     session_destroy();
     header("Location: /auth");
     exit;
    }

    public function nopermissions() {
        $this->setMessages('No permission for operation.');
        header("Location: ".$_SERVER['HTTP_REFERER']);
        exit;
    }

    public function register() {
        if(!$this->isPost()) {
            return $this->render('register');
        }

        $userRepository = new UsersRepository();
        $checkUser = $userRepository->getUserLogin($_POST['email']);

        if($checkUser) {
            $this->setMessages('Account with this email already exists.');
            header("Location: ".$_SERVER['HTTP_REFERER']);
            exit;
        }

        $user = new User(
            $_POST['email'],
            $_POST['username'],
            2,
            NULL,
            $_POST['password']);

        $userRepository->addUser($user);
        header('Location: /auth/login');
        exit;

    }
}