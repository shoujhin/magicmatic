<?php

require_once __DIR__.'//..//repository//CardsRepository.php';
require_once 'AppController.php';

class CardsController extends AppController
{
    private $cardRepository;


    public function __construct()
    {
        $this->userPermission = 1;
        parent::__construct();
        $this->cardRepository = new CardsRepository();
    }

    public function view($id)
    {
        if(empty($id)) {
            header('Location: '.'/cards/');
            exit;

        }
        $this->render('layout', [
            'card'=>$this->cardRepository->getCard($id),
            'action' => 'view',
            'viewName' =>  'card_edit'
        ]);
    }

    public function index()
    {
        $this->render('layout', ['cards'=>$this->cardRepository->getCards(), 'viewName' =>  'card_index' ] );
    }

    public function add()
    {
        $this->checkPermission($this->userPermission);

        if($this->isPost()) {
            $card = new Card(
                $_POST['cardname'],
                $_POST['set_id'],
                $_POST['is_white'],
                $_POST['is_red'],
                $_POST['is_black'],
                $_POST['is_green'],
                $_POST['is_blue'],
                $_POST['price']
            );

            $this->cardRepository->addCard($card);
            $this->setMessages('Card has been added.');
            header('Location: '.'/cards/');
            exit;
        }

        $this->render('layout', [
            'card'=>$this->cardRepository->getCard(0)
            ,'action' => 'add'
            ,'viewName' => 'card_edit'
        ]);

    }

    public function delete($id) {
        $this->checkPermission($this->userPermission);
        if($this->cardRepository->beforeDeleteCard($id)) {
            $this->setMessages('Card cannot be deleted.');
            header('Location: '.'/cards/');
            exit;
        }
        $this->cardRepository->deleteCard($id);
        $this->setMessages('Card has been deleted.');
        header('Location: '.'/cards/');
        exit;
    }

    public function edit($id)
    {
        $this->checkPermission($this->userPermission);
        if(empty($id)) {
            header('Location: '.'/cards/');
            exit;
        }
        if($this->isPost()) {
            $this->cardRepository->editCard($_POST);
            header('Location: '.'/cards/');
            exit;
        }

        $this->render('layout', [
            'card'=>$this->cardRepository->getCard($id)
            ,'action' => 'edit'
            ,'viewName' => 'card_edit'
        ]);

    }

}