<?php
require_once __DIR__.'//..//repository//UserDecksRepository.php';
require_once 'AppController.php';

class UserDecksController extends AppController
{
    private $userDecksRepository;

    public function __construct()
    {
        parent::__construct();
        $this->userDecksRepository = new UserDecksRepository();
    }

    public function index()
    {
        $this->render('layout', [
            'user_decks'=>$this->userDecksRepository->getUserDecks($this->getAuthUserId()),
            'my_decks'=>true,
            'viewName' =>  'user_deck_index',
            ] );
    }

    public function all()
    {
        $this->render('layout', [
            'user_decks'=>$this->userDecksRepository->getUserDecks(),
            'my_decks'=>false,
            'viewName' =>  'user_deck_index'
        ] );
    }

    public function delete($id) {
        $deck = $this->userDecksRepository->getUserDeck($id);
        if($this->getAuthUserId()!=$deck->getUserId()) {
            $this->setMessages('No permission for this operation.');
        } else {
            $this->userDecksRepository->deleteUserDeck($id);
            $this->setMessages('Deck has been deleted.');
        }
        header('Location: '.$_SERVER['HTTP_REFERER']);
        exit;
    }

    public function add()
    {
        if($this->isPost()) {
            $userDeck = new UserDeck(
                $_POST['deckname'],
                $this->getAuthUserId()
            );

            $this->userDecksRepository->addUserDeck($userDeck);
            $this->setMessages('Deck has been added.');
            header('Location: '.'/user_decks/');
            exit;
        }

        $this->render('layout', [
            'action' => 'add'
            ,'viewName' => 'user_deck_edit'
        ]);

    }

    public function copy($deckId)
    {
        $deck = $this->userDecksRepository->getUserDeck($deckId);
        $newDeck = new UserDeck(
          $deck->getDeckname().' copy',
          $this->getAuthUserId()
        );
        $newDeckId = $this->userDecksRepository->addUserDeck($newDeck);
        $this->userDecksRepository->copyDeck($deckId, $newDeckId);
        $this->setMessages('Deck has been copied.');
        header('Location: '.'/user_decks/');
        exit;
    }

    public function edit($id)
    {

        if(empty($id) && !$this->isPost()) {
            header('Location: '.'/user_decks/');
            exit;
        }

        if(!empty($id)) {
            $deck = $this->userDecksRepository->getUserDeck($id);
        } else {
            $deck = $this->userDecksRepository->getUserDeck($_POST['id']);
        }


        if($this->getAuthUserId()!=$deck->getUserId())
        {
            $this->setMessages('No permission for this operation.');
            header('Location: '.'/user_decks/');
            exit;
        }

        if($this->isPost()) {
            $deck->setDeckname($_POST['deckname']);
            $this->userDecksRepository->editUserDeck($deck);
            header('Location: '.'/user_decks/');
            exit;
        }



        $this->render('layout', [
            'user_deck'=>$this->userDecksRepository->getUserDeck($id)
            ,'action' => 'edit'
            ,'viewName' => 'user_deck_edit'
        ]);

    }
}