<?php

require_once __DIR__.'//..//repository//UsersRepository.php';
require_once 'AppController.php';

class UsersController extends AppController
{
    private $userRepository;

    public function __construct()
    {
        $this->userPermission = 1;
        $this->checkPermission($this->userPermission);
        parent::__construct();
        $this->userRepository = new UsersRepository();
    }

    public function view($id)
    {
        if(empty($id)) {
            header('Location: '.'/users/');
            exit;

        }
        $this->render('layout', [
            'user'=>$this->userRepository->getUser($id),
            'action' => 'view',
            'viewName' =>  'user_edit'
        ]);
    }

    public function index()
    {
        $this->render('layout', ['users'=>$this->userRepository->getUsers(), 'viewName' =>  'user_index' ] );
    }

    public function add()
    {
        if($this->isPost()) {
            $user = new User(
                $_POST['email'],
                $_POST['username'],
                $_POST['role_id'],
                NULL,
                $_POST['password']);

            $this->userRepository->addUser($user);
            header('Location: '.'/users/');
            exit;
        }

        $this->render('layout', [
            'user'=>$this->userRepository->getUser(0)
            ,'action' => 'add'
            ,'viewName' => 'user_edit'
        ]);

    }

    public function delete($id) {
        $this->userRepository->deleteUser($id);
        header('Location: '.'/users/');
        exit;
    }

    public function edit($id)
    {
        if(empty($id)) {
            header('Location: '.'/users/');
            exit;
        }
        if($this->isPost()) {
            $this->userRepository->editUser($_POST);
            header('Location: '.'/users/');
            exit;
        }

        $this->render('layout', [
            'user'=>$this->userRepository->getUser($id)
            ,'action' => 'edit'
            ,'viewName' => 'user_edit'
        ]);

    }
}