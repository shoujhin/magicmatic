#Magicmatic

###General Info
Zadaniem aplikacji jest przechowywanie, edytowanie, wyszukiwanie decków do gry Magic: The Gathering.

###Description
Strona główna umożliwia założenie konta, a następnie zalogowanie się do aplikacji.
Po zalogowaniu się zostajemy przeniesieni na stronę ze wszystkimi dostępnymi kartami do składania talii.
W zakładce All decks możemy przeglądać talie wszystkich graczy oraz tworzyć ich kopię przypisane do naszego konta.
W zakładce My decks znajdują się utworzone przez nas talie oraz talie skopiowane od innych graczy, można je tam edytować.

![](screens/1.png)![](screens/2.png)![](screens/3.png)![](screens/4.png)![](screens/5.png)![](screens/6.png)