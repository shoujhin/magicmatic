<?php

session_start();

require 'Router.php';

$path = trim($_SERVER['REQUEST_URI'], '/');
$path = parse_url($path, PHP_URL_PATH);
$server = "http://$_SERVER[HTTP_HOST]";

Router::get('', 'SecurityController');
Router::get('cards', 'CardsController');
Router::get('users', 'UsersController');
Router::get('user_decks', 'UserDecksController');
Router::get('deck_cards', 'DeckCardsController');
Router::get('main', 'DefaultController');
Router::get('register', 'DefaultController');
Router::get('dashboard', 'DefaultController');
Router::post('auth', 'SecurityController');

Router::run($path);