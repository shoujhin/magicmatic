<?php
require_once 'src/controllers/BasicFunctions.php';

require_once 'src/controllers/DefaultController.php';
require_once 'src/controllers/SecurityController.php';
require_once 'src/controllers/CardsController.php';
require_once 'src/controllers/UsersController.php';
require_once 'src/controllers/UserDecksController.php';
require_once 'src/controllers/DeckCardsController.php';

class Router
{
    public static $routes;

    public static function get($url, $view)
    {
        self::$routes[$url] = $view;
    }

    public static function post($url, $view)
    {
        self::$routes[$url] = $view;
    }

    public static function run($url)
    {
        $path = explode("/", $url);
        $controller = $path[0];

        if($controller != 'auth') {
            if(!isset($_SESSION['auth'])) {
                header("Location: http://$_SERVER[HTTP_HOST]/auth");
                exit;
            }
        }

        if (!array_key_exists($controller, self::$routes)) {
            die("Wrong url!");
        }

        $controllerAction = self::$routes[$controller];

        if (isset($path[1])) {
            $action = explode("/", $url)[1];
        } else {
            $action = 'index';
        }

        $param = NULL;
        if(isset($path[2])) {
            $param = $path[2];
        }
        $object = new $controllerAction;

        $object->$action($param);
    }
}