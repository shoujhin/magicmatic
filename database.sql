create table cards
(
    id       serial
        constraint cards_pk
            primary key,
    cardname varchar(255)          not null,
    set_id   integer               not null,
    is_white boolean default false not null,
    is_red   boolean default false not null,
    is_black boolean default false not null,
    is_green boolean default false not null,
    is_blue  boolean default false not null,
    price    double precision
);

alter table cards
    owner to msqzmxspqyragc;

create unique index cards_id_uindex
    on cards (id);

INSERT INTO public.cards (id, cardname, set_id, is_white, is_red, is_black, is_green, is_blue, price) VALUES (22, 'Zhur-Taa Goblin', 3, false, true, false, true, false, 5);
INSERT INTO public.cards (id, cardname, set_id, is_white, is_red, is_black, is_green, is_blue, price) VALUES (23, 'Gruul Guildmage', 1, false, true, false, true, false, 3);
INSERT INTO public.cards (id, cardname, set_id, is_white, is_red, is_black, is_green, is_blue, price) VALUES (24, 'Wrecking Beast', 4, false, false, false, true, false, 20);
INSERT INTO public.cards (id, cardname, set_id, is_white, is_red, is_black, is_green, is_blue, price) VALUES (25, 'Jayce Beleren', 3, false, false, false, false, true, 50);
INSERT INTO public.cards (id, cardname, set_id, is_white, is_red, is_black, is_green, is_blue, price) VALUES (26, 'Niv-Mizzet, Parun', 1, false, true, false, false, true, 15);
INSERT INTO public.cards (id, cardname, set_id, is_white, is_red, is_black, is_green, is_blue, price) VALUES (27, 'Doomskar', 2, true, false, false, false, false, 7);
INSERT INTO public.cards (id, cardname, set_id, is_white, is_red, is_black, is_green, is_blue, price) VALUES (28, 'Zimone, Quandrix Prodigy', 3, false, false, false, true, true, 100);
INSERT INTO public.cards (id, cardname, set_id, is_white, is_red, is_black, is_green, is_blue, price) VALUES (29, 'Dead Presence', 2, false, false, true, false, false, 5);
INSERT INTO public.cards (id, cardname, set_id, is_white, is_red, is_black, is_green, is_blue, price) VALUES (30, 'Puppet Raiser', 4, false, false, true, false, false, 6);
INSERT INTO public.cards (id, cardname, set_id, is_white, is_red, is_black, is_green, is_blue, price) VALUES (31, 'Sick Strength', 4, false, true, false, false, false, 3);
INSERT INTO public.cards (id, cardname, set_id, is_white, is_red, is_black, is_green, is_blue, price) VALUES (32, 'Terror of the Peaks', 2, false, true, false, false, false, 10);
INSERT INTO public.cards (id, cardname, set_id, is_white, is_red, is_black, is_green, is_blue, price) VALUES (34, 'Destruction of Empire', 2, false, true, false, false, false, 3);
INSERT INTO public.cards (id, cardname, set_id, is_white, is_red, is_black, is_green, is_blue, price) VALUES (36, 'Become Immense', 2, false, false, false, true, false, 3);




create table decks_cards
(
    id       serial
        constraint decks_cards_pk
            primary key,
    deck_id  integer not null,
    card_id  integer not null,
    quantity integer not null
);

alter table decks_cards
    owner to msqzmxspqyragc;

create unique index decks_cards_id_uindex
    on decks_cards (id);

INSERT INTO public.decks_cards (id, deck_id, card_id, quantity) VALUES (13, 8, 31, 3);
INSERT INTO public.decks_cards (id, deck_id, card_id, quantity) VALUES (14, 8, 32, 2);
INSERT INTO public.decks_cards (id, deck_id, card_id, quantity) VALUES (16, 8, 34, 4);
INSERT INTO public.decks_cards (id, deck_id, card_id, quantity) VALUES (17, 9, 23, 3);
INSERT INTO public.decks_cards (id, deck_id, card_id, quantity) VALUES (18, 9, 22, 4);
INSERT INTO public.decks_cards (id, deck_id, card_id, quantity) VALUES (19, 9, 24, 1);
INSERT INTO public.decks_cards (id, deck_id, card_id, quantity) VALUES (23, 11, 28, 1);
INSERT INTO public.decks_cards (id, deck_id, card_id, quantity) VALUES (24, 11, 26, 3);
INSERT INTO public.decks_cards (id, deck_id, card_id, quantity) VALUES (25, 11, 24, 4);
INSERT INTO public.decks_cards (id, deck_id, card_id, quantity) VALUES (26, 11, 25, 3);



create table roles
(
    id       serial
        constraint roles_pk
            primary key,
    rolename varchar(255) not null
);

alter table roles
    owner to msqzmxspqyragc;

INSERT INTO public.roles (id, rolename) VALUES (1, 'Admin');
INSERT INTO public.roles (id, rolename) VALUES (2, 'User');




create table users
(
    id       serial
        constraint users_pk
            primary key,
    username varchar(100) not null,
    email    varchar(255) not null,
    password varchar(100) not null,
    role_id  integer      not null
);

alter table users
    owner to msqzmxspqyragc;

create unique index users_email_uindex
    on users (email);

create unique index users_id_uindex
    on users (id);

INSERT INTO public.users (id, username, email, password, role_id) VALUES (1, 'admin', 'arek.skrzypek@gmail.com', 'e10adc3949ba59abbe56e057f20f883e', 1);
INSERT INTO public.users (id, username, email, password, role_id) VALUES (6, 'TestUser', 'test@gmail.com', 'e10adc3949ba59abbe56e057f20f883e', 2);




create table users_decks
(
    id            integer   default nextval('user_decks_id_seq'::regclass) not null
        constraint user_decks_pk
            primary key,
    deckname      varchar(100)                                             not null,
    user_id       integer                                                  not null,
    creation_date timestamp default CURRENT_TIMESTAMP,
    is_white      boolean   default false                                  not null,
    is_red        boolean   default false                                  not null,
    is_black      boolean   default false                                  not null,
    is_green      boolean   default false                                  not null,
    is_blue       boolean   default false                                  not null
);

alter table users_decks
    owner to msqzmxspqyragc;

INSERT INTO public.users_decks (id, deckname, user_id, creation_date, is_white, is_red, is_black, is_green, is_blue) VALUES (8, 'Mono red aggro', 1, '2022-02-01 15:11:24.115660', false, true, false, false, false);
INSERT INTO public.users_decks (id, deckname, user_id, creation_date, is_white, is_red, is_black, is_green, is_blue) VALUES (9, 'Gruul', 1, '2022-02-01 15:16:59.469417', false, true, false, true, false);
INSERT INTO public.users_decks (id, deckname, user_id, creation_date, is_white, is_red, is_black, is_green, is_blue) VALUES (11, 'Commander', 6, '2022-02-01 15:21:52.960990', false, true, false, true, true);



create function delete_deck_cards() returns trigger
    language plpgsql
as
$$
BEGIN
    DELETE FROM decks_cards WHERE deck_id = OLD.id;

    RETURN OLD;

END;
$$;

alter function delete_deck_cards() owner to msqzmxspqyragc;





create function set_deck_color() returns trigger
    language plpgsql
as
$$
DECLARE
    is_white_ int;
    is_red_ int;
    is_black_ int;
    is_green_ int;
    is_blue_ int;

    id_ int;
BEGIN

    IF (TG_OP = 'INSERT') THEN

        SELECT
            MAX(is_white::int) ,
            MAX(is_red::int) ,
            MAX(is_black::int) ,
            MAX(is_green::int) ,
            MAX(is_blue::int)
        into is_white_, is_red_, is_black_, is_green_, is_blue_
        FROM decks_cards d
                 left join cards c on c.id = d.card_id
        WHERE deck_id = NEW.deck_id;

        UPDATE users_decks
        SET
            is_white = is_white_::bool,
            is_red = is_red_::bool,
            is_black = is_black_::bool,
            is_green = is_green_::bool,
            is_blue = is_blue_::bool
        WHERE id = NEW.deck_id;

        RETURN NEW;

    ELSIF (TG_OP = 'DELETE') THEN
        SELECT
            MAX(is_white::int) ,
            MAX(is_red::int) ,
            MAX(is_black::int) ,
            MAX(is_green::int) ,
            MAX(is_blue::int)
        into is_white_, is_red_, is_black_, is_green_, is_blue_
        FROM decks_cards d
                 left join cards c on c.id = d.card_id
        WHERE deck_id = OLD.deck_id;

        UPDATE users_decks
        SET
            is_white = is_white_::bool,
            is_red = is_red_::bool,
            is_black = is_black_::bool,
            is_green = is_green_::bool,
            is_blue = is_blue_::bool
        WHERE id = OLD.deck_id;

        RETURN OLD;
    END IF;
    RETURN NULL;


END;
$$;

alter function set_deck_color() owner to msqzmxspqyragc;



create trigger update_decks_cards
    after insert or delete
    on decks_cards
    for each row
execute procedure set_deck_color();


create trigger delete_decks_cards
    after delete
    on users_decks
    for each row
execute procedure delete_deck_cards();









